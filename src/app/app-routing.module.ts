import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './block/content/content.component';

const routes: Routes = [
  {
    path: '', component: ContentComponent, children: [
      { path: 'workspaces', loadChildren: () => import('./feature/workspaces/workspaces.module').then(m => m.WorkspacesModule) },
      { path: 'filevault', loadChildren: () => import('./feature/filevault/filevault.module').then(m => m.FilevaultModule) },
      { path: 'users', loadChildren: () => import('./feature/users/users.module').then(m => m.UsersModule) },
      { path: 'template-form', loadChildren: () => import('./feature/template-form/template-form.module').then(m => m.TemplateFormModule) },
      { path: 'reactive-form', loadChildren: () => import('./feature/reactive-form/reactive-form.module').then(m => m.ReactiveFormModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
