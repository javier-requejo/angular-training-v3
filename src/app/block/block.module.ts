import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContentComponent } from './content/content.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { VersionNumberComponent } from './footer/version-number/version-number.component';


@NgModule({
  declarations: [
    SidenavComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    VersionNumberComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ]
})
export class BlockModule { }
