import { Attribute, ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-version-number',
  template: '<div>Version {{version}}</div>',
  styleUrls: ['./version-number.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VersionNumberComponent implements OnInit {

  constructor(@Attribute('version') readonly version = '0.0.0') { }

  ngOnInit(): void {
  }

}
