import { Component, OnInit } from '@angular/core';

const NAMES = ['javi', 'roland', 'roberto', 'colin', 'adam'];
const EMAILS = ['javi@veratrak.com', 'roland@veratrak.com', 'roberto@veratrak.com', 'colin@veratrak.com', 'adam@veratrak.com'];

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  username: string = 'javi';
  email: string = 'javi@veratrak.com';
  
  constructor() { }

  ngOnInit(): void {
  }
  
  changeUsername(): void {
    this.username = NAMES[Math.floor(Math.random() * NAMES.length)];
  }
  
  changeEmail(): void {
    this.email = EMAILS[Math.floor(Math.random() * EMAILS.length)];
  }
  
  doNothing(): void {
    console.log('Do nothing');
  }

}
