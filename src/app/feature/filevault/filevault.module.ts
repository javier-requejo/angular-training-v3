import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilevaultComponent } from './filevault.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: FilevaultComponent }
];

@NgModule({
  declarations: [FilevaultComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class FilevaultModule { }
