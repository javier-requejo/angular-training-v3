import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-dropdown',
  templateUrl: './profile-dropdown.component.html',
  styleUrls: ['./profile-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileDropdownComponent implements OnInit {

  @Input() username = '';
  @Input() email = '';
  
  constructor() { }

  ngOnInit(): void {
  }
  
  get detectionHappened(): boolean {
    console.log('hi! detection happened');
    return true;
  }

}
