import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileDropdownComponent } from './profile-dropdown/profile-dropdown.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BellComponent } from './bell/bell.component';
import { CardComponent } from './card/card.component';

const components = [
  ProfileDropdownComponent,
  BellComponent,
  CardComponent,
];

const modules = [
  MatIconModule,
  MatMenuModule,
  MatSidenavModule,
  MatListModule,
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule
];

@NgModule({
  declarations: components,
  exports: [components, ...modules],
  imports: [CommonModule, ...modules],
})
export class SharedModule { }
